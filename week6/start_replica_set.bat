@echo off
REM remove the directories
rd /s /q C:\data\rs1 C:\data\rs2 C:\data\rs3
REM create them
md C:\data\rs1 C:\data\rs2 C:\data\rs3
start "rs1" /min mongod --replSet m101 --logpath "log\1.log" --dbpath C:\data\rs1 --port 27017 --smallfiles
start "rs2" /min mongod --replSet m101 --logpath "log\2.log" --dbpath C:\data\rs2 --port 27018 --smallfiles
start "rs3" /min mongod --replSet m101 --logpath "log\3.log" --dbpath C:\data\rs3 --port 27019 --smallfiles
