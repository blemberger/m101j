db.grades.aggregate([
	/* Flatten the scores array into individual score objects */
	{$unwind: '$scores'},
	/* Exclude all documents where the score type is 'quiz' */
	{$match: 
			{'scores.type': {$ne: 'quiz'}}
	},
	/* Find average score of each student in each class */
   	{$group:
		   	{_id:
					{student_id: '$student_id',
					 class_id: '$class_id'},
			 student_avg: {$avg: '$scores.score'}
			}
	},
	{$group:
			{_id: '$_id.class_id',
			 class_avg: {$avg: '$student_avg'}
			}
	},
	{$sort: {class_avg: -1}}
	])
