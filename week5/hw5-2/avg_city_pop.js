db.zips.aggregate([
	{$match: 
			{state: {$in: ['CA', 'NY']}}
	},
   	{$group:
		   	{_id:
				   	{city: '$city',
				  	 state: '$state'},
			 population: {$sum: '$pop'}
			}
	},
	{$match:
			{population: {$gt: 25000}}
	},
	{$group:
			{_id: null,
			 avg_pop_city: {$avg: '$population'}
			}
	}
	])
