db.zips.aggregate([
	/* Exclude all documents where the city name does not start with a digit */
	{$match: 
		{city: {$regex: '^\[0-9\]'}}	
	},
	/* Find total population from each such document */
   	{$group:
		   	{_id:	null,
			 total_rural_pop: {$sum: '$pop'}
			}
	}
	])
