var types = ['rock', 'paper', 'scissors'];
var colors = ['red', 'green', 'blue'];
for (var i=0; i<5000000; i++) {
	var typeObjs = [];
	for (j=0; j<3; j++) {
		var type = types[j % 3];
		typeObjs[j] = {'type': type, color: colors[Math.floor(Math.random()*3)]};
	}
	var obj = {name: 'thing_' + i, disposition: 'thingy', types: typeObjs, 
				weight: Math.floor(Math.random()*10000)/100};
	db.things.save(obj);
}
