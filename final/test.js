db.stuff.aggregate([{$group:{_id:'$c'}}])

db.messages.aggregate([{$group: {_id: '$headers.To'}}])

db.messages.aggregate([{$group: {_id: '$headers.From'}}])

db.messages.aggregate([
    {$unwind: '$headers.To'},
    {$group: 
        {_id: 
            {from: '$headers.From',
             to: '$headers.To'
            },
         count: {$sum: 1}
        }
    },
    {$sort: {count: -1}},
    {$limit: 10}
])

db.messages.update(
    {'headers.Message-ID': '<8147308.1075851042335.JavaMail.evans@thyme>'},
    {$addToSet: 
        {
            'headers.To': 'mrpotatohead@10gen.com'
        }
    }
)

