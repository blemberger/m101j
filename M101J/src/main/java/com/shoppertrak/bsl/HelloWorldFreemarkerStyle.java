package com.shoppertrak.bsl;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class HelloWorldFreemarkerStyle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Configuration configuration = new Configuration();
		configuration.setClassForTemplateLoading(HelloWorldFreemarkerStyle.class, "/");
		
		try {
			Template helloTemplate = configuration.getTemplate("hello.ftl");
			StringWriter writer = new StringWriter();
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("name", "Freemarker");
			helloTemplate.process(model, writer);
			System.out.println(writer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
