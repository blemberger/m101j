package com.shoppertrak.bsl;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

public class HelloWorldMongoDBStyle {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws UnknownHostException {
		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));
		DB theDb = client.getDB("mine");
		DBCollection collection = theDb.getCollection("hello");
		DBObject document = collection.findOne();
		System.out.println("The document: " + document);
	}

}
