package com.shoppertrak.bsl;

import java.io.StringWriter;
import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import freemarker.template.Configuration;
import freemarker.template.Template;

public class HelloWorldMongoDBSparkFreemarkerStyle {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws UnknownHostException {
		final Configuration configuration = new Configuration();
		configuration.setClassForTemplateLoading(HelloWorldMongoDBSparkFreemarkerStyle.class, "/");
		
		
		MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));
		DB theDb = client.getDB("test");
		final DBCollection collection = theDb.getCollection("bslcollection");
		
		Spark.get(new Route("/") {
			@Override
			public Object handle(Request req, Response resp) {
				StringWriter writer = new StringWriter();
				try {
					Template helloTemplate = configuration.getTemplate("hello.ftl");
					
					DBObject document = collection.findOne();

					helloTemplate.process(document, writer);
				} catch (Exception e) {
					e.printStackTrace();
					halt(500);
				}
				return writer;
			}
		});

	}

}
