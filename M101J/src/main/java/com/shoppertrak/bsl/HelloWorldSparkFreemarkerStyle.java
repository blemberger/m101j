package com.shoppertrak.bsl;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class HelloWorldSparkFreemarkerStyle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final Configuration configuration = new Configuration();
		configuration.setClassForTemplateLoading(HelloWorldSparkFreemarkerStyle.class, "/");
		Spark.get(new Route("/") {

			@Override
			public Object handle(Request req, Response resp) {
				StringWriter writer = new StringWriter();
				try {
					Template helloTemplate = configuration.getTemplate("hello.ftl");

					Map<String, Object> model = new HashMap<String, Object>();
					model.put("name", "Freemarker");
					helloTemplate.process(model, writer);
				} catch (Exception e) {
					e.printStackTrace();
					halt(500);
				}
				return writer;
			}
		});

	}

}
