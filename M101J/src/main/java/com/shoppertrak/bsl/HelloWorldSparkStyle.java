package com.shoppertrak.bsl;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class HelloWorldSparkStyle {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Spark.get(new Route("/") {

			@Override
			public Object handle(Request req, Response resp) {
				return "Hello World from Spark!";
			}
			
		});

	}

}
